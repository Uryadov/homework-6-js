function filterBy(array, filterTypeof) {
    return array.filter(function (item) {
        if (typeof item !== filterTypeof) {
            return true;
        }
    });
}

console.log(filterBy(['string', 'pen', 44, 2, 55, false, null, undefined], 'string'));

